## Prime Number Algorithms ##

These are various implementations of prime number algorithms. They are designed
to output the results of the algorithms to the command line. However, each can be
easily modified to return the prime number sets for calculation.


