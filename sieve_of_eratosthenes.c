#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


void sieveOfEratosthenes(const int n)
{
    int x, y, i = 0;
    int limit = sqrt(n);

    /* Primary array that will store primes. */
    char *pList = NULL;
    pList = malloc(sizeof(char*)*(n+1));

    /*
     * Initialize pList with 1's, denoting prime by default.
     * pList[0] and pList[1] are set to 0 as 0 and 1 are not primes.
     */
    memset(pList, 1,(n+1));
    pList[0];
    pList[1];

    for ( y=2; y <= limit; y++)
    {
        /* pList is prime by default. If no changes are made then it is a prime. */
        if (pList[y] == 1)
        {
            /* Multiples are not prime update them to 0 */
            for (i = y*2; i <= n; i += y)
            {
                pList[i] = 0;
            }
        }
    }

    /* Print prime numbers to stdout. */
    for (x = 2; x <= n; x++)
    {
        if (pList[x])
        {
            printf("%d\n", x);
        }
    }

    /* Free memory. */
    free(pList);
}