#include <stdio.h>
#include <stdlib.h>
#include "palgo.h"

int trialDivision(int num) {

    int isPrime = 0;
    int x = 0;

    if (num <= 1)
    {
        printf("Not prime.");

        return 0;

    } else {

        for (int i = 2; i < num; ++i)
        {
            isPrime = 0;
            x = i/2;

            for (int j = 2; j < x; ++j)
            {
                if ((i % j) == 0)
                {
                    isPrime = 1;
                    break;
                }
            }

            if (isPrime == 0)
            {
                printf("%d %s\n", i, " is prime.");
            }
        }
    }

    exit(EXIT_SUCCESS);
}

