
#ifndef PRIME_ALGORITHMS_PALGO_H
#define PRIME_ALGORITHMS_PALGO_H

int trialDivision(int);
void sieveOfEratosthenes(const int);

#endif //PRIME_ALGORITHMS_PALGO_H
